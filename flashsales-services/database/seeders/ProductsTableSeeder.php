<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create(
            [
                "product_name" => "Nike Air Zoom Terra Triger K8 - Size 40",
                "product_qty" => 3,
                "product_information" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Donec at augue tortor. Ut iaculis vel est ut dictum. Vestibulum imperdiet urna nec ullamcorper sollicitudin",
                "product_image" => "http://localhost:8080/images/air-zoom-terra-kiger.jpg",
                "product_price" => 2000
            ]
        );
        Product::create(
            [
                "product_name" => "Dior Medium Lady DLite",
                "product_qty" => 5,
                "product_information" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Donec at augue tortor. Ut iaculis vel est ut dictum. Vestibulum imperdiet urna nec ullamcorper sollicitudin",
                "product_image" => "http://localhost:8080/images/medium-lady-dlite.jpg",
                "product_price" => 2900
            ]
        );
        Product::create(
            [
                "product_name" => "Large Dior Book Tote",
                "product_qty" => 7,
                "product_information" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Donec at augue tortor. Ut iaculis vel est ut dictum. Vestibulum imperdiet urna nec ullamcorper sollicitudin",
                "product_image" => "http://localhost:8080/images/M1286ZTDT_M808_TU.jpg",
                "product_price" => 2700
            ]
        );
        Product::create(
            [
                "product_name" => "Dior Bobby East-West Bag",
                "product_qty" => 6,
                "product_information" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Donec at augue tortor. Ut iaculis vel est ut dictum. Vestibulum imperdiet urna nec ullamcorper sollicitudin",
                "product_image" => "http://localhost:8080/images/M9327UMOL_M37O_TU.jpg",
                "product_price" => 2700
            ]
        );
        Product::create(
            [
                "product_name" => "Micro 30 Montaigne Bag",
                "product_qty" => 9,
                "product_information" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Donec at augue tortor. Ut iaculis vel est ut dictum. Vestibulum imperdiet urna nec ullamcorper sollicitudin",
                "product_image" => "http://localhost:8080/images/S2110UMOS_M057_TU.jpg",
                "product_price" => 1900
            ]
        );
        Product::create(
            [
                "product_name" => "Micro Lady Dior Bag",
                "product_qty" => 6,
                "product_information" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Donec at augue tortor. Ut iaculis vel est ut dictum. Vestibulum imperdiet urna nec ullamcorper sollicitudin",
                "product_image" => "http://localhost:8080/images/S0856ONGE_M27Y_TU.jpg",
                "product_price" => 2800
            ]
        );
        Product::create(
            [
                "product_name" => "Micro Lady Dior Bag",
                "product_qty" => 9,
                "product_information" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Donec at augue tortor. Ut iaculis vel est ut dictum. Vestibulum imperdiet urna nec ullamcorper sollicitudin",
                "product_image" => "http://localhost:8080/images/S0856ONGE_M68H_TU.jpg",
                "product_price" => 2600
            ]
        );
    }
}
