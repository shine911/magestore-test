<?php

namespace Database\Seeders;

use App\Models\FlashSale;
use App\Models\FlashSaleDetails;
use App\Models\Product;
use Illuminate\Database\Seeder;

class FlashSalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $now = date("Y-m-d h:i:s");
        $daystart = date("Y-m-d h:i:s", strtotime($now. '+ 30 second'));
        $sevendaysafter = date("Y-m-d h:i:s",strtotime($now. '+ 7 days'));
        
        $flashsale = FlashSale::create([
            "flashsale_name" => "Flash Sale 04/04",
            "flashsale_start" => $daystart,
            "flashsale_end" => $sevendaysafter,
            "flashsale_status" => true,
        ]);

        $products = Product::all();
        foreach($products as $product) {
            FlashSaleDetails::create([
                "product_id" => $product->product_id,
                "flashsale_id" => $flashsale->flashsale_id,
                "product_original_price" => $product->product_price,
                "product_flashsale_price" => (0.8 * $product->product_price),
                "product_flashsale_qty" => $product->product_qty
            ]);
        }
    }
}
