<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(){
        Schema::create('orders', function(Blueprint $table) {
            $table->increments('order_id');
            $table->string('order_fullname');
            $table->string('order_phone');
            $table->string('order_address');
            $table->integer('product_id');
            $table->double('product_price');
            $table->double('order_total_price');
            $table->integer('order_status');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('orders');
    }
};