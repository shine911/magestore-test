<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(){
        Schema::create('flashsales', function(Blueprint $table){
            $table->increments('flashsale_id');
            $table->string('flashsale_name');
            $table->dateTime('flashsale_start');
            $table->dateTime('flashsale_end');
            $table->boolean('flashsale_status');
            $table->timestamps();
        });
        Schema::create('flashsale_details', function(Blueprint $table){
            $table->increments('flashsale_detail_id');
            $table->integer('product_id');
            $table->integer('flashsale_id');
            $table->double('product_original_price');
            $table->double('product_flashsale_price');
            $table->integer('product_flashsale_qty');
            $table->timestamps();
        });
    }
    public function down(){
        Schema::drop('flashsales');
        Schema::drop('flashsale_details');
    }
};