<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(){
        Schema::create('products', function(Blueprint $table) {
            $table->increments('product_id');
            $table->string('product_name');
            $table->integer('product_qty');
            $table->double('product_price');
            $table->string('product_information');
            $table->string('product_image');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('products');
    }
};