<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(["prefix"=>"v1"], function() use ($router){
    //get information method
    $router->get('get-product-list', ["uses" => 'FlashSalesController@getProductList']);
    $router->get('get-flashsale-list', ["uses" => "FlashSalesController@getFlashSalesList"]);
    $router->get('get-product-on-sale', ["uses" => 'FlashSalesController@getFlashSalesDetailsProduct']);
    $router->get('get-orders', ["uses" => 'FlashSalesController@getOrders']);
    $router->get('get-lastest-flashsale', ['uses' => 'FlashSalesController@getFlashSaleDetail']);

    //post method
    $router->post('buy-product-action', ['uses' => 'FlashSalesController@actionBuy']);

    //put method


    //delete method
});