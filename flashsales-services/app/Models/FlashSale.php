<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FlashSale extends Model {
    protected $table = "flashsales";
    protected $primaryKey = 'flashsale_id';
}