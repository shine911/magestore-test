<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FlashSaleDetails extends Model {
    protected $table = "flashsale_details";
    protected $primaryKey = 'flashsale_detail_id';
}