<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {
    protected $table = "orders";
    protected $primaryKey = 'order_id';
    protected $fillable = ["order_fullname", "order_phone", "order_address", "product_id", "product_price", "order_total_price", "order_status"];
}