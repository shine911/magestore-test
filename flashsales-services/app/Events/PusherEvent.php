<?php

namespace App\Events;
use Pusher\Pusher;

class PusherEvent extends Event
{
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($event, $message)
    {
        //
        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            [
                'cluster' => env('PUSHER_APP_CLUSTER'),
                'useTLS'=> FALSE
            ]
        );
        $pusher->trigger('my-channel', $event, $message);
    }
}
