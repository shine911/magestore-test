<?php

namespace App\Jobs;

use App\Events\PusherEvent;
use App\Models\FlashSale;
use App\Models\FlashSaleDetails;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class OrderPodcast extends Job
{
    protected $flashsale_detail;
    protected $order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(FlashSaleDetails $flashsale_detail, Order $order)
    {
        //
        $this->flashsale_detail = $flashsale_detail;
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $flashsale_detail = $this->flashsale_detail;
        $order = $this->order;
        // $now = strtotime(date("Y-m-d h:i:s"));
        // $flashsale = FlashSale::where("flashsale_id", $flashsale_detail->flashsale_id)->get()[0];
        // $start_date = strtotime($flashsale->flashsale_start);
        // if($now >= $start_date){
            if($flashsale_detail->product_flashsale_qty >= 1 ) {
                $flashsale_detail->product_flashsale_qty = $flashsale_detail->product_flashsale_qty - 1;
                $flashsale_detail->save();
                $product = Product::where("product_id", $flashsale_detail->product_id)->get()[0];
                $qty = $product->product_qty;
                $product->product_qty = --$qty;
                $product->save();
                $order->order_status = 1;
                $order->save();
                $message = ["type"=>"success", "message" => "Your order is successfully"];
                event(new PusherEvent('notification', $message));
            } else {
                $order->order_status = 2;
                $order->save();
                $message = ["type"=>"error", "message" => "Your order is missing out"];
                event(new PusherEvent('notification', $message));
            }
        // } else {
        //     $order = $this->order;
        //     $order->order_status = 3;
        //     $order->save();
        // }
    }

    public function failed()
    {
        $order = $this->order;
        $order->order_status = 3;
        $order->save();
    }
}
