<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Events\PusherEvent;
use App\Jobs\OrderPodcast;
use App\Models\FlashSale;
use App\Models\FlashSaleDetails;
use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FlashSalesController extends Controller {

    public function getFlashSalesList(){
        $flashsales = FlashSale::all();
        return $flashsales;
    }

    public function getProductList() {
        $product = Product::all();
        return $product;
    }

    public function actionSubmitProductSales(Request $request) {
        $productList = json_decode($request->input("flashsale_details_list"));
        $productDelete = json_decode($request->input("flashsale_details_delete_list"));
        DB::transaction(function () use ($productList, $productDelete) {
            foreach($productList as $product) {
                $product->save();
            }
            foreach($productDelete as $product) {
                $product->delete();
            }
        });
        return "Products Update Successfully";        
    }

    public function getFlashSalesDetailsProduct() {
        $flashsales = FlashSale::first();
        $sql = "select pd.*, fd.product_flashsale_price, fd.flashsale_detail_id from flashsale_details fd join products pd on fd.product_id = pd.product_id where fd.flashsale_id = ?";
        $products = DB::select($sql, [$flashsales->flashsale_id]);
        return $products;
    }

    public function getFlashSaleDetail(){
        $flashsales = FlashSale::first();
        return $flashsales;
    }

    public function getOrders(Request $request){
        $phone = $request->input('phone');
        $sql = "select * from orders ord join products pd on pd.product_id = ord.product_id where ord.order_phone = ? order by ord.created_at desc";
        $order = DB::select($sql, [$phone]);
        return $order;
    }

    public function actionBuy(Request $request) {
        $user = (object) $request->input("user");
        $flashsale_detail_id = $request->input("flashsale_detail");
        $flashsale_detail = FlashSaleDetails::where("flashsale_detail_id", $flashsale_detail_id)->first();
        $order = Order::create([
            "order_fullname" => $user->full_name,
            "order_phone" => $user->phone,
            "order_address" => $user->address,
            "product_id" => $flashsale_detail->product_id,
            "product_price" => $flashsale_detail->product_original_price,
            "order_total_price" => $flashsale_detail->product_flashsale_price,
            "order_status" => 0
        ]);
        //Dispatch orders
        dispatch(new OrderPodcast($flashsale_detail, $order));
        return "Order is validating by system";
    }
}