export const state = () => ({
  products: [],
  orders: [],
  orderModal: false,
  user: {
    full_name: '',
    address: '',
    phone: ''
  }
})
export const mutations = {
  saveProducts (state, products) {
    state.products = products
  },
  saveUser (state, user) {
    state.user = user
  },
  saveOrders (state, orders) {
    state.orders = orders
  },
  toggleOrderModal (state, toggle) {
    state.orderModal = toggle
  }
}
